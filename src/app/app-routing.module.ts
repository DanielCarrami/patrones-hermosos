import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioDashboardComponent } from './pages-components/inicio-dashboard/inicio-dashboard.component';
import { AboutPageComponent } from './pages-components/about-page/about-page.component'
import { ParticipaPageComponent } from './pages-components/participa-page/participa-page.component';
import { InscribetePageComponent } from './pages-components/inscribete-page/inscribete-page.component';
import { NoticiasPageComponent } from './pages-components/noticias-page/noticias-page.component';
import { ContactoPageComponent } from './pages-components/contacto-page/contacto-page.component';
import { EquipoPageComponent } from './pages-components/equipo-page/equipo-page.component';
import { PatrocinadoresPageComponent } from './pages-components/patrocinadores-page/patrocinadores-page.component';
import { LoginPageComponent } from './pages-components/login-page/login-page.component';
import { NoticiaPageComponent } from './pages-components/noticia-detalle-page/noticia-detalle-page.component';
import { RegisterPageComponent } from './pages-components/register-page/register-page.component';
import { AdminPageComponent } from './pages-components/admin-page/admin-page.component';
import { SedeFormComponent } from './core/forms/sede-form/sede-form.component';
import { AsesorFormComponent } from './core/forms/instructora-form/instructora-form.component';
import { AsistenteFormComponent } from './core/forms/asistente-form/asistente-form.component';

const routes: Routes = [
  {path: '', component: InicioDashboardComponent },
  {path: 'inicio', component: InicioDashboardComponent },
  {path: 'about', component: AboutPageComponent},
  {path: 'participa', component: ParticipaPageComponent},
  {path: 'inscribete', component: InscribetePageComponent},
  {path: 'noticias', component: NoticiasPageComponent},
  {path: 'contacto', component: ContactoPageComponent},
  {path: 'conoce-al-equipo', component: EquipoPageComponent},
  {path: 'patrocinadores-y-colaboradores', component: PatrocinadoresPageComponent},
  {path: 'login', component: LoginPageComponent },
  {path: 'register', component: RegisterPageComponent },
  {path: 'admin', component: AdminPageComponent },
  {path: 'noticia-detalle', component: NoticiaPageComponent},
  {path: 'formulario-sede', component: SedeFormComponent},
  {path: 'formulario-sede/:id', component: SedeFormComponent},
  {path: 'formulario-asistente', component: AsistenteFormComponent},
  {path: 'formulario-asistente/:id', component: AsistenteFormComponent},
  {path: 'formulario-instructora', component: AsesorFormComponent},
  {path: 'formulario-instructora/:id', component: AsesorFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
