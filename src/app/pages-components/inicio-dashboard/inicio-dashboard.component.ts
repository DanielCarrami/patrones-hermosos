import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';

import {UltimasNoticiasComponent} from '../../core/ultimas-noticias/ultimas-noticias.component';
import {Router} from '@angular/router'

@Component({
  selector: 'app-inicio-dashboard',
  templateUrl: './inicio-dashboard.component.html',
  styleUrls: ['./inicio-dashboard.component.css']
})
export class InicioDashboardComponent implements OnInit {

  noticias = [];
  noticias1 ;
  noticias2 ;
  noticias3 ;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    
    this.authService.Allnews()
    .subscribe(
      res =>{
        this.noticias = res;
        this.noticias1 = res.length - 1;
        this.noticias2 = res.length-2;
        this.noticias3 = res.length-3;

      }
    )

  }

  enviarnoticia(id){
    localStorage.setItem('noticia',id);
    this.router.navigate(['/noticia-detalle']);
  }
}
