import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipaPageComponent } from './participa-page.component';

describe('ParticipaPageComponent', () => {
  let component: ParticipaPageComponent;
  let fixture: ComponentFixture<ParticipaPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipaPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipaPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
