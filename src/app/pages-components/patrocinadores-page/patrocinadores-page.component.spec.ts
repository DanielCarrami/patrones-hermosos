import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatrocinadoresPageComponent } from './patrocinadores-page.component';

describe('PatrocinadoresPageComponent', () => {
  let component: PatrocinadoresPageComponent;
  let fixture: ComponentFixture<PatrocinadoresPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatrocinadoresPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatrocinadoresPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
