import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router'

@Component({
  selector: 'app-noticia-detalle-page',
  templateUrl: './noticia-detalle-page.component.html',
  styleUrls: ['./noticia-detalle-page.component.css']
})
export class NoticiaPageComponent implements OnInit {

  
  noticias = [];

  comentarioborrar = {
    noticia_id: 0,
    id: 0
    
  }

  comentariomandar = {
    noticia_id: 0,
    contenido:''
  }

  respuesta = {
    noticia_id: 0,
    id: 0,
    contenido:''
  }

  agregarvisita = {
    id: 0,
    visitas:0
  }
  numerorespuesta = []

  noticiamostrar = {
    titulo:'',
    autor: '',
    contenido: '',
    fecha: '',
    imagen:'',
    comentarios:[],
    id: 0,
    visitas: 0,
    likes:0
  };
  comentarios = [];
  noticias1 ;
  noticias2 ;
  noticias3 ;

  agregarlike = {
    id: 0,
    likes:0
  }

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {

    const noticia = {
      id: localStorage.getItem('noticia')
    }

    
    this.authService.getOneNews(noticia)
      .subscribe(
        res => {
          this.noticiamostrar = res;
          var division = this.noticiamostrar.fecha;
          var splitted = division.split("T",2);
          this.noticiamostrar.fecha = splitted[0];
          this.comentarios = this.noticiamostrar.comentarios;
          this.comentariomandar.noticia_id = this.noticiamostrar.id;
          this.comentarioborrar.noticia_id = this.noticiamostrar.id;
          this.respuesta.noticia_id = this.noticiamostrar.id;
          this.agregarvisita.id = this.noticiamostrar.id;
          this.agregarvisita.visitas = this.noticiamostrar.visitas + 1;
          this.numerorespuesta = this.noticiamostrar.comentarios[0].respuesta;
          
          
          this.authService.updateViews(this.agregarvisita)
          .subscribe(
          res =>{
          
          }
         )
        
        },
        err => console.log(err)
      )

      this.authService.Allnews()
      .subscribe(
        res =>{
          
          this.noticias = res;
          this.noticias1 = res.length - 1;
          this.noticias2 = res.length-2;
          this.noticias3 = res.length-3;
  
        }
      )

      
  }

  enviarnoticia(id){
    localStorage.setItem('noticia',id);
    location.reload();
  }

  iniciarSesion(){
    this.router.navigate(['/login']);
  }

  likear(id,numlikes){
    this.agregarlike.id = id;
    this.agregarlike.likes = numlikes + 1;

    this.authService.updatelike(this.agregarlike)
      .subscribe()
    location.reload()
  }

  login(){
    this.router.navigate(['/login']);
  }

  loggedIn(){
    return !!localStorage.getItem('token');
  }

  comentar(){
    this.authService.comentar(this.comentariomandar)
    .subscribe(
      err => console.log(err)
    )

    location.reload();
  }

  
  borrar_comentario(comentario_id){

    this.comentarioborrar.id = comentario_id;
    this.authService.deletecomment(this.comentarioborrar)
    .subscribe(
      err => console.log(err)
    )

    location.reload();

  }

  todasnoticias(){
    this.router.navigate(['/noticias']);
  }

  adminverify(){
    if(localStorage.getItem("admin") != null){
      return true;
    }
    else{
      return false;
    }
  }

  respuestaempty(){
    
    if(this.numerorespuesta.length > 0){
      return true;
    }
    else{
      return false;
    }
  }

  responder(comentario_id){
    this.respuesta.id = comentario_id;
    this.authService.respuestacomment(this.respuesta)
    .subscribe(
      err => console.log(err)
    )
  }

  formato_fecha(date): string{
    const months = {
      '01': 'ene',
      '02': 'feb',
      '03': 'mar',
      '04': 'abr',
      '05': 'may',
      '06': 'jun',
      '07': 'jul',
      '08': 'ago',
      '09': 'sep',
      '10': 'oct',
      '11': 'nov',
      '12': 'dic'
    }

    let newDate = date.substring(8,10) + ' de ' + months[date.substring(5,7)] +' del ' + date.substring(0, 4);

    return newDate;
  }


}
