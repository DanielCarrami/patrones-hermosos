import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router'
import {FailLoginComponent} from '../../core/dialogs/fail-login/fail-login.component';
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  user = {
    email: '',
    clave: ''
  }

  succes = false;

  constructor(
    private authService: AuthService, 
    private router: Router,
    private dialog: MatDialog) { }

  ngOnInit(): void {
    this.authService.showusers().subscribe(res=>console.log(res))
  }


  backClicked() {
    this.router.navigate(['/inicio']);
  }
  
  signUp(){
    this.authService.signIn(this.user)
      .subscribe(
        res => {
            if(res.confirmado === false){
              this.openFailDialog();
            }else{
                localStorage.setItem('token',res.token);
              if (res.admin === true){
                localStorage.setItem('admin',res.admin);
              }
            }
            this.router.navigate(['/inicio']);
        },
        err => {
          this.succes = true;
          console.log(err);}
      )
  }
  logOut(){
     this.authService.logout()
  }

  openFailDialog(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    this.dialog.open(FailLoginComponent, dialogConfig);
    this.router.navigate(['/inicio']);
  }

}
