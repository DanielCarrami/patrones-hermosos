import { Component, OnInit } from '@angular/core';

import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';

@Component({
  selector: 'app-contacto-page',
  templateUrl: './contacto-page.component.html',
  styleUrls: ['./contacto-page.component.css']
})


export class ContactoPageComponent implements OnInit {

  constructor() { }

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  nombreControl = new FormControl('', [
    Validators.required
  ]);

  asuntoControl = new FormControl('', [
    Validators.required
  ]);

  mensajeControl = new FormControl('', [
    Validators.required
  ]);
 
  mensaje = {
    nombre: '',
    email: '',
    asunto: '',
    contenido:''
  }
  ngOnInit(): void {
  }

  verify(): boolean{
    if(this.mensaje.nombre === '' || this.mensaje.email === '' || this.mensaje.asunto === '' || this.mensaje.contenido=== ''){
      return false;
    }
    return true;
  }

  submit(): void{
    if(this.verify()){
      console.log(this.mensaje);
    }
  }
}
