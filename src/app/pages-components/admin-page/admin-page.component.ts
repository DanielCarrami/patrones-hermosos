import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {FormControl, Validators} from '@angular/forms';


@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {

  constructor(private router: Router) { }

  show_sedes = true;
  show_instructoras = false;
  show_asistentes = false;

  show_noticias = true;
  show_usuarios = false;
  show_admin = false;

  ngOnInit(): void {
    if(localStorage.getItem("admin") == null){
      this.router.navigate(['/inicio']);
    }
  }

  show_sede(){
    this.show_sedes = true;
    this.show_instructoras = false;
    this.show_asistentes = false;
  }

  show_instructora(){
    this.show_sedes = false;
    this.show_instructoras = true;
    this.show_asistentes = false;
  }

  show_asistente(){
    this.show_sedes = false;
    this.show_instructoras = false;
    this.show_asistentes = true;
  }

  show_noticia_table(){
    this.show_noticias = true;
    this.show_usuarios = false;
    this.show_admin = false;
  }

  show_usuarios_table(){
    this.show_noticias = false;
    this.show_usuarios = true;
    this.show_admin = false;
  }

  show_agregar_admin(){
    this.show_noticias = false;
    this.show_usuarios = false;
    this.show_admin = true;
  }
}
