import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InscribetePageComponent } from './inscribete-page.component';

describe('InscribetePageComponent', () => {
  let component: InscribetePageComponent;
  let fixture: ComponentFixture<InscribetePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InscribetePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InscribetePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
