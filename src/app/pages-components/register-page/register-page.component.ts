import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router'


import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {FormControl, Validators} from '@angular/forms';
import {RegisterSuccessComponent} from '../../core/dialogs/register-success/register-success.component';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {
  user = {
    email: '',
    clave: '',
    nombre:'',
    apellido: ''
  }

  errors = {
    passwordMatch: null,
    notEmpty: null
  }

  clave2: string;

  constructor(
    private authService: AuthService, 
    private router: Router,
    private dialog: MatDialog) { }
  
  mensaje_error = '';

  existente = false;
  
  emailControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  nombreControl = new FormControl('', [
    Validators.required
  ]);

  apellidoControl = new FormControl('', [
    Validators.required
  ]);

  passwordControl = new FormControl('', [
    Validators.required
  ]);

  ngOnInit(): void {
  }

  verifyEmpy(): boolean{
    if(this.user.nombre === '' || this.user.email === '' || this.user.clave === '' || this.user.apellido === ''){
      this.errors.notEmpty = 'Todos los campos deben ser llenados'
      return false;
    }
    this.errors.notEmpty = null;
    return true;
  }

  registrarse(){
    if(this.verifyEmpy){
      if(this.clave2 === this.user.clave){
        this.authService.agregarusuario(this.user)
        .subscribe(
        res =>{
          this.openSuccessDialog();
        }
      )
      }else{
        this.errors.passwordMatch = 'Las contraseñas deben coincidir';
      }
    }
  }

  openSuccessDialog(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      name: this.user.nombre + ' ' + this.user.apellido
    }

    this.dialog.open(RegisterSuccessComponent, dialogConfig);
    this.router.navigate(['/inicio']);
  }
}
