import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarnoticiasPageComponent } from './agregarnoticias-page.component';

describe('AgregarnoticiasPageComponent', () => {
  let component: AgregarnoticiasPageComponent;
  let fixture: ComponentFixture<AgregarnoticiasPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarnoticiasPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarnoticiasPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
