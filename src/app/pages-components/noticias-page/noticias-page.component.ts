import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import { MatInputModule } from '@angular/material/input';
import {Router} from '@angular/router'

@Component({
  selector: 'app-noticias-page',
  templateUrl: './noticias-page.component.html',
  styleUrls: ['./noticias-page.component.css']
})
export class NoticiasPageComponent implements OnInit {

 
  

  constructor(private authService: AuthService, private router: Router) { }

  busqueda = true;
  titulonoticia = {
    titulo: ''
  }
  noticiasbusqueda = [];
  noticias;

  agregarlike = {
    id: 0,
    likes:0
  }


  ngOnInit(): void {
    this.authService.AllnewsOrdenadas()
    .subscribe(
      res =>{
        this.noticias = res;
        for(let i = 0; i < this.noticias.length;i++){
          var division = this.noticias[i].fecha;
          var splitted = division.split("T",2);
          this.noticias[i].fecha = splitted[0];
        }
        

      }
    )

  }

  buscar(){
    this.busqueda = false;
    this.authService.busquedaNoticia(this.titulonoticia)
    .subscribe(
      res =>{
        this.noticiasbusqueda = res;
        for(let i = 0; i < this.noticiasbusqueda.length;i++){
          var division = this.noticiasbusqueda[i].fecha;
          var splitted = division.split("T",2);
          this.noticiasbusqueda[i].fecha = splitted[0];
        }

      }
    )

  }

  enviarnoticia(id){
    localStorage.setItem('noticia',id);
    this.router.navigate(['/noticia-detalle']);
  }

  likear(id,numlikes){
    this.agregarlike.id = id;
    this.agregarlike.likes = numlikes + 1;

    this.authService.updatelike(this.agregarlike)
      .subscribe(
        res =>{
  
        }
      )

    location.reload()
  }



}
