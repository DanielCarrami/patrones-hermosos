export class Tutor {
    constructor(
        public id: number,
        public nombre: String,
        public email: String,
        public telefono: String,
        public nombre_alt: String,
        public email_alt: String,
        public asistente_id: number
    ){

    }
}