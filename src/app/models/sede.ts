export class Sede {
    constructor(
        public id: number,
        public nombre_institucion: String,
        public estado: String,
        public nivel: String,
        public campus: String,
        public direccion: String,
        public participo_antes: boolean,
        public labo_disp: boolean,
        public no_participantes: number,
        public salones_disp: boolean,
        public no_profesores: number,
        public exp_hospedaje: String,
        public dist_sede: number,
        public cantidad_asistentes: number,
        public compromiso_alimentos: boolean,
        public compromiso_fondos: boolean,
        public compromiso_transportar: boolean,
        public compromiso_permisos: boolean,
        public compromiso_apoyo: boolean,
        public compromiso_fotos: boolean,
        public aceptar_datos: boolean,
        public comentarios: String,
        public autorizado: String,
    ){

    }
}