export class Teaching {
    constructor(
        public id: number,
        public nombre: String,
        public apellido: String,
        public email: String,
        public telefono: String,
        public edad: number,
        public aviso_privacidad: boolean,
        public estado: String,
        public ciudad: String,
        public carrera: String,
        public anio_graduación: String,
        public universidad: String,
        public disp_cambio_sede: boolean,
        public semestre_carrera: String,
        public razon_participacion: String,
        public experiencias_previa: boolean,
        public razon_carrera: String,
        public disponibilidad_hospedaje: boolean,
        public sede_id: number,
        public autorizado: String,
    ){

    }
}