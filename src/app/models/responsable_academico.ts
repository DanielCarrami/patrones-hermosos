export class Responsable_academico {
    constructor(
        public id: number,
        public nombre: String,
        public email: String,
        public telefono: String,
        public nombre_alt: String,
        public email_alt: String,
        public sede_id: number
    ){

    }
}