export class Asistente {
    constructor(
        public id: number,
        public nombre: String,
        public apellido: String,
        public email: String,
        public direccion: String,
        public telefono: String,
        public fecha_de_nacimiento: Date,
        public escuela: String,
        public tipo_escuela: String,
        public pais: String,
        public estado: String,
        public ciudad: String,
        public municipio: String,
        public codigo_postal: String,
        public grado: String,
        public promedio: number,
        public como_entro: String,
        public talla: String,
        public participo_antes: boolean,
        public clase_previa: boolean,
        public estudiar_tic: boolean,
        public aviso_privacidad: boolean,
        public autorizacion_medios: boolean,
        public tiene_laptop: boolean,
        public estatura: number,
        public nombre_diploma: String,
        public sede_id: number,
        public autorizado: String,
    ){

    }
}