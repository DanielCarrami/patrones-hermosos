import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-redes-sociales',
  templateUrl: './redes-sociales.component.html',
  styleUrls: ['./redes-sociales.component.css']
})
export class RedesSocialesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  send(option): void{
    switch(option){
      case 'F': 
      window.open("https://www.facebook.com/patroneshermosos");
      break;
      case 'T': 
      window.open("https://twitter.com/Patroneshermoso");
      break;
      case 'I': 
      window.open("https://www.instagram.com/patroneshermosositson/?hl=es-la");
      break;
      case 'Y': 
      window.open("https://www.youtube.com/watch?v=wHJ-u9au4BQ");
      break;
    }
  
  }
}
