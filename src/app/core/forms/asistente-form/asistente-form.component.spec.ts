import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsistenteFormComponent } from './asistente-form.component';

describe('AsistenteFormComponent', () => {
  let component: AsistenteFormComponent;
  let fixture: ComponentFixture<AsistenteFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsistenteFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsistenteFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
