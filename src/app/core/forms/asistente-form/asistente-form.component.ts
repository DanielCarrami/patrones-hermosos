import { Component, OnInit, destroyPlatform } from '@angular/core';
import {FormGroup,FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { CrudService, Model } from '../../../services/crud.service';
import {Router, ActivatedRoute} from '@angular/router'
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {NotifySuccessComponent} from '../../dialogs/notify-success/notify-success.component';
import {WarningInscripcionComponent} from '../../dialogs/warning-inscripcion/warning-inscripcion.component';

interface Options {
  value: string;
  viewValue: string;
} 

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-asistente-form',
  templateUrl: './asistente-form.component.html',
  styleUrls: ['./asistente-form.component.css']
})
export class AsistenteFormComponent implements OnInit {

  selectedValueTipo: string;
  selectedValueTalla: string;
  selectedValueSede: string;
  selectedValueEstado: string;
  selectedValueGrad: string;

  entra = true;

  no_edit: boolean = true;
  id: any = '';

  profileForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
  });

  tipos: Options[] = [
    {value: 'Privada', viewValue: 'Privada'},
    {value: 'Pública', viewValue: 'Pública'}
  ];

  grados: Options[] = [
    {value: 'Primero', viewValue: 'Primero de preparatoria'},
    {value: 'Segundo', viewValue: 'Segundo de preparatoria'},
    {value: 'Tercero', viewValue: 'Tercero de preparatoria'}
  ];

  tallas: Options[] = [
    {value: 'XS', viewValue: 'Extra Chica'},
    {value: 'S', viewValue: 'Chica'},
    {value: 'M', viewValue: 'Mediana'},
    {value: 'G', viewValue: 'Grande'},
    {value: 'XG', viewValue: 'Extra Grande'},
    {value: 'XXL', viewValue: 'Extra Extra Grande'}
  ];
  sedes: Options[] = [];
  estados: Options[] = [
    {value: 'Otro', viewValue: 'Otro'},
    {value: 'Aguascalientes', viewValue: 'Aguascalientes'},
    {value: 'Baja California', viewValue: 'Baja California'},
    {value:'Baja California Sur',viewValue: 'Baja California Sur'},
    {value: 'Campeche',viewValue: 'Campeche'},
    {value: 'Chihuahua',viewValue: 'Chihuahua'},
    {value: 'Chiapas',viewValue:'Chiapas'},
    {value: 'Ciudad de México', viewValue: 'Ciudad de México'},
    {value: 'Coahuila',viewValue: 'Coahuila'},
    {value: 'Colima',viewValue: 'Colima'},
    {value: 'Durango',viewValue: 'Durango'},
    {value: 'Estado de México', viewValue: 'Estado de México'},
    {value: 'Guanajuato',viewValue: 'Guanajuato'},
    {value: 'Guerrero', viewValue: 'Guerrero'},
    {value: 'Hidalgo', viewValue: 'Hidalgo'},
    {value: 'Jalisco', viewValue: 'Jalisco'},
    {value: 'Michoacán', viewValue: 'Michoacán'},
    {value: 'Morelos',viewValue:'Morelos'},
    {value: 'Nayarit',viewValue: 'Nayarit'},
    {value: 'Nuevo León', viewValue: 'Nuevo León'},
    {value: 'Nayarit',viewValue: 'Nayarit'},
    {value: 'Puebla',viewValue: 'Puebla'},
    {value: 'Querétaro',viewValue: 'Querétaro'},
    {value: 'Quintana Roo',viewValue: 'Quintana Roo'},
    {value: 'San Luis Potosi',viewValue: 'San Luis Potosi'},
    {value: 'Sinaloa', viewValue: 'Sinaloa'},
    {value: 'Sonora', viewValue: 'Sonora'},
    {value: 'Tabasco',viewValue: 'Tabasco'},
    {value: 'Tamaulipas', viewValue: 'Tamaulipas'},
    {value: 'Tlaxcala',viewValue: 'Tlaxcala'},
    {value: 'Veracrúz',viewValue: 'Veracrúz'},
    {value: 'Yucatán', viewValue: 'Yucatán'},
    {value: 'Zacatecas',viewValue: 'Zacatecas'},
  ];

  asistente = {
    nombre: '',
    apellido: '',
    email: '',
    direccion:'',
    ciudad:'',
    estado:'',
    codigo_postal:'',
    pais: '',
    telefono:'',
    fecha_de_nacimiento: '',
    escuela:'',
    tipo_escuela: '',
    municipio:'',
    grado: '',
    promedio:null,
    participo_antes:false,
    clase_previa: '',
    estudiar_tic: false,
    como_entro: '',
    talla: '',
    estatura:'',
    aviso_privacidad: false,
    autorizacion_medios: false,
    sede_id: '',
    tiene_laptop: false,
    nombre_diploma: ''

  }

  tutor = {
    nombre:'',
    email:'',
    telefono:'',
    asistente_id: ''
  }


  matcher = new MyErrorStateMatcher();
  constructor(
    private crudService: CrudService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog
  ) { }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.warn(this.profileForm.value);
  }

  ngOnInit(): void {
    this.crudService.get_sede_view(Model.SEDE)
    .then(res =>{
      this.sedes = res.data.map(sede => {
        return {
          value: sede.id,
          viewValue: sede.nombre_institucion
        }
      })
    })
    .catch(err => {
      console.error('EERRROOOOOOOORRRR ' + err);
    });

    this.id = this.route.snapshot.paramMap.get('id');
      if(this.id != undefined){
        this.no_edit = false;
        this.crudService.retrieve(Model.ASISTENTE, this.id)
        .then(res => this.asistente = res.data)
        .catch(err => console.error(err))
      }
  }

  openErrorDialog(err, info){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      error: err,
      info: info
    }

    this.dialog.open(WarningInscripcionComponent, dialogConfig);
    this.router.navigate(['/inicio']);
  }

  openDialog(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      model: "Asistente",
      name: this.asistente.nombre + ' ' + this.asistente.apellido
    }

    this.dialog.open(NotifySuccessComponent, dialogConfig);
  }

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  nombre = new FormControl('', [
    Validators.required
  ]);

  apellido = new FormControl('', [
    Validators.required
  ]);

  direccion = new FormControl('', [
    Validators.required
  ]);

  ciudad = new FormControl('', [
    Validators.required
  ]);

  estado = new FormControl('', [
    Validators.required
  ]);

  municipio = new FormControl('', [
    Validators.required
  ]);


  codigo_postal = new FormControl('', [
    Validators.required
  ]);

  pais = new FormControl('', [
    Validators.required
  ]);

  telefono = new FormControl('', [
    Validators.required
  ]);

  fecha_de_nacimiento = new FormControl('', [
    Validators.required
  ]);

  escuela = new FormControl('', [
    Validators.required
  ]);

  tipo_escuela = new FormControl('', [
    Validators.required
  ]);

  grado = new FormControl('', [
    Validators.required
  ]);

  clase_previa = new FormControl('', [
    Validators.required
  ]);

  como_entra = new FormControl('', [
    Validators.required
  ]);

  talla = new FormControl('', [
    Validators.required
  ]);

  estatura = new FormControl('', [
    Validators.required, Validators.max(3.00), Validators.min(1.00)
  ]);

  promedio = new FormControl('', [
    Validators.required, Validators.max(100.00), Validators.min(0.00)
  ]);

  sede_id = new FormControl('', [
    Validators.required
  ]);

  nombre_diploma = new FormControl('', [
    Validators.required
  ]);

  tutor_nombre = new FormControl('', [
    Validators.required
  ]);

  tutor_telefono = new FormControl('', [
    Validators.required
  ]);

  tutor_asistente_id = new FormControl('', [
    Validators.required
  ]);


  
  verificador(){
    if(this.asistente.nombre === '' ||this.asistente.apellido === '' || this.asistente.direccion === '' || this.asistente.email === '' || this.asistente.ciudad === '' || this.asistente.estado === '' ||this.asistente.codigo_postal === '' ||this.asistente.pais === '' ||this.asistente.telefono === '' ||this.asistente.fecha_de_nacimiento === '' ||this.asistente.escuela === '' ||this.asistente.tipo_escuela === '' ||this.asistente.municipio === '' ||this.asistente.grado === '' ||this.asistente.clase_previa === '' ||this.asistente.como_entro === '' ||this.asistente.talla === '' ||this.asistente.estatura === '' ||this.asistente.aviso_privacidad == false ||this.asistente.autorizacion_medios == false ||this.asistente.nombre_diploma === '' || this.asistente.promedio == 0 ){
      this.entra = false;
    } else{
      this.entra = true;
    }
  }

  ingresar(){
    
    if(this.no_edit){
        this.verificador()
      if(this.entra){
        this.crudService.create(Model.ASISTENTE, this.asistente)
        .then(res => {this.tutor.asistente_id = res.data.id;
        this.crudService.create(Model.TUTOR, this.tutor)
      .then(res => 
        this.openDialog()
      )
    .catch(err => this.openErrorDialog('Lamentamos informarte que hubo un error inesperado en tu inscripción, intentalo de nuevo más tarde', ''))})
        .catch(err => {  
          this.openErrorDialog('La información capturada ya fue previamente registrada, revise su correo.', this.asistente.nombre + ' ' + this.asistente.apellido);
          });
        
        this.router.navigate(['/inicio']);
      }
    }else{
      this.crudService.update(Model.ASISTENTE, this.id, this.asistente)
      .catch(err => console.error(err))
      this.router.navigate(['/admin']);
    }
  }

  destroy(){
      this.crudService.delete(Model.ASISTENTE, this.id).catch(err => console.error(err));
  }

}
