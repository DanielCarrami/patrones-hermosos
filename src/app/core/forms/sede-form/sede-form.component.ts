import { Component, OnInit } from '@angular/core';
import { CrudService, Model } from '../../../services/crud.service';
import {FormGroup,FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import {Router} from '@angular/router'
import { ActivatedRoute} from '@angular/router';
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {NotifySuccessComponent} from '../../dialogs/notify-success/notify-success.component';

interface Options {
  value: string;
  viewValue: string;
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-sede-form',
  templateUrl: './sede-form.component.html',
  styleUrls: ['./sede-form.component.css']
})

export class SedeFormComponent implements OnInit {
  selectedValueEstado: string;
  selectedValueNivel: string;
  selectedValueCampus: string;

  entra = true;
  id: any = '';
  no_edit: boolean = true;

  estados: Options[] = [
    {value: 'Aguascalientes', viewValue: 'Aguascalientes'},
    {value: 'Baja California', viewValue: 'Baja California'},
    {value:'Baja California Sur',viewValue: 'Baja California Sur'},
    {value: 'Campeche',viewValue: 'Campeche'},
    {value: 'Chihuahua',viewValue: 'Chihuahua'},
    {value: 'Chiapas',viewValue:'Chiapas'},
    {value: 'Ciudad de México', viewValue: 'Ciudad de México'},
    {value: 'Coahuila',viewValue: 'Coahuila'},
    {value: 'Colima',viewValue: 'Colima'},
    {value: 'Durango',viewValue: 'Durango'},
    {value: 'Estado de México', viewValue: 'Estado de México'},
    {value: 'Guanajuato',viewValue: 'Guanajuato'},
    {value: 'Guerrero', viewValue: 'Guerrero'},
    {value: 'Hidalgo', viewValue: 'Hidalgo'},
    {value: 'Jalisco', viewValue: 'Jalisco'},
    {value: 'Michoacán', viewValue: 'Michoacán'},
    {value: 'Morelos',viewValue:'Morelos'},
    {value: 'Nayarit',viewValue: 'Nayarit'},
    {value: 'Nuevo León', viewValue: 'Nuevo León'},
    {value: 'Nayarit',viewValue: 'Nayarit'},
    {value: 'Puebla',viewValue: 'Puebla'},
    {value: 'Querétaro',viewValue: 'Querétaro'},
    {value: 'Quintana Roo',viewValue: 'Quintana Roo'},
    {value: 'San Luis Potosi',viewValue: 'San Luis Potosi'},
    {value: 'Sinaloa', viewValue: 'Sinaloa'},
    {value: 'Sonora', viewValue: 'Sonora'},
    {value: 'Tabasco',viewValue: 'Tabasco'},
    {value: 'Tamaulipas', viewValue: 'Tamaulipas'},
    {value: 'Tlaxcala',viewValue: 'Tlaxcala'},
    {value: 'Veracrúz',viewValue: 'Veracrúz'},
    {value: 'Yucatán', viewValue: 'Yucatán'},
    {value: 'Zacatecas',viewValue: 'Zacatecas'},
  ];

  niveles: Options[] = [
    {value: 'Privada',viewValue: 'Privada'},
    {value: 'Pública',viewValue: 'Pública'},

  ];

  campus: Options[] = [
    {value: 'Campus Chihuahua',viewValue: 'Campus Chihuahua'},
    {value: 'Campus Ciudad de México',viewValue: 'Campus Ciudad de México'},
    {value: 'Campus Cuernavaca',viewValue: 'Campus Cuernavaca'},
    {value: 'Campus Estado de México',viewValue: 'Campus Estado de México'},
    {value: 'Campus Guadalajara',viewValue: 'Campus Guadalajara'},
    {value: 'Campus León',viewValue: 'Campus León'},
    {value: 'Campus Monterrey',viewValue: 'Campus Monterrey'},
    {value: 'Campus Puebla',viewValue: 'Campus Puebla'},
    {value: 'Campus Querétaro',viewValue: 'Campus Querétaro'},
    {value: 'Campus San Luis Potosí',viewValue: 'Campus San Luis Potosí'},
    {value: 'Campus Santa Fé',viewValue: 'Campus Santa Fé'},
  ];

  profileForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
  });

  constructor(
    private crudService: CrudService, 
    private router: Router, 
    private route: ActivatedRoute,
    private dialog: MatDialog) { }

  sede={
    estado:'',
    nombre_institucion:'',
    nivel:'',
    campus:'',
    direccion:'',
    participo_antes:false,
    no_participantes: '',
    labo_disp:false,
    salones_disp:false,
    no_profesores:'',
    exp_hospedaje: '',
    dist_sede:'',
    cantidad_asistentes:'',
    compromiso_alimentos:false, 
    compromiso_fondos: false,
    compromiso_transportar: false,
    compromiso_permisos:false,
    compromiso_apoyo:false,
    compromiso_fotos: false,
    aceptar_datos:false,
    comentarios:''
  } 

  responsables_academicos={
    email:'',
    nombre:'',
    telefono:'',
    email_alt:'',
    nombre_alt:'',
    sede_id: ''
  }

  captacion={
    email:'',
    nombre:'', 
    telefono:'',
    sede_id: ''
  }

  programas_internacionales={
    email:'',
    nombre:'',
    telefono:'',
    email_alt:'',
    nombre_alt:'',
    sede_id: ''
  }
  
  matcher = new MyErrorStateMatcher();

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    if(this.id != undefined){
      this.no_edit = false;
      this.crudService.retrieve(Model.SEDE, this.id)
      .then(res => {
        this.sede = res.data
      })
      .catch(err=> console.error(err))
    }
  }

  openDialog(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      model: "Sede",
      name: this.sede.nombre_institucion
    }

    this.dialog.open(NotifySuccessComponent, dialogConfig);
  }

  emailFormControl1 = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  emailFormControl2 = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  emailFormControl3 = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  emailFormControl4 = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  emailFormControl5 = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  nombre_institucion = new FormControl('',[
    Validators.required
  ])

  estado = new FormControl('',[
    Validators.required
  ])

  nivel = new FormControl('',[
    Validators.required
  ])

  campus_val = new FormControl('',[
    Validators.required
  ])

  direccion = new FormControl('',[
    Validators.required
  ])

  no_participantes = new FormControl('',[
    Validators.required
  ])

  no_profesores = new FormControl('',[
    Validators.required
  ])

  exp_hospedaje = new FormControl('',[
    Validators.required
  ])

  dist_sede = new FormControl('',[
    Validators.required
  ])

  cantidad_asistentes = new FormControl('',[
    Validators.required
  ])

  responsables_email = new FormControl('',[
    Validators.required
  ])

  responsables_nombre = new FormControl('',[
    Validators.required
  ])

  responsables_telefono = new FormControl('',[
    Validators.required
  ])

  responsables_sede_id = new FormControl('',[
    Validators.required
  ])

  captacion_email = new FormControl('',[
    Validators.required
  ])

  captacion_nombre = new FormControl('',[
    Validators.required
  ])

  captacion_telefono = new FormControl('',[
    Validators.required
  ])

  captacion_sede_id = new FormControl('',[
    Validators.required
  ])

  programas_nombre = new FormControl('',[
    Validators.required
  ])

  programas_telefono = new FormControl('',[
    Validators.required
  ])


  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.warn(this.profileForm.value);
  }

  verificador(){
    if(this.sede.nombre_institucion === '' || this.sede.estado === '' || this.sede.nivel === '' || this.sede.campus === '' || this.sede.direccion === '' || this.sede.no_participantes === '' || this.sede.no_profesores === '' || this.sede.exp_hospedaje === '' || this.sede.dist_sede === ''|| this.sede.cantidad_asistentes === ''|| this.sede.comentarios === '' || this.responsables_academicos.email === ''|| this.responsables_academicos.nombre === ''|| this.responsables_academicos.telefono === ''|| this.captacion.email === '' || this.captacion.nombre === '' || this.captacion.telefono === '' ){
       this.entra = false;
    } else{
       this.entra = true;
    }
  }

  ingresar(){
    if(this.no_edit){
        this.verificador()
      
      if(this.entra == true){
        this.crudService.create(Model.SEDE, this.sede)
        .then(res => {
        this.responsables_academicos.sede_id = res.data.id;
        this.crudService.create(Model.RESPONSABLE_ACADEMICO, this.responsables_academicos);
        this.captacion.sede_id = res.data.id;
        this.crudService.create(Model.CONTACTO_CAPTACION, this.captacion);
        this.programas_internacionales.sede_id = res.data.id;
        this.crudService.create(Model.PROGRAMAS_INTERNACIONALES, this.programas_internacionales);
      });
      this.openDialog();
      this.router.navigate(['/inicio']);
      }
    }else{
      this.crudService.update(Model.SEDE, this.id, this.sede)
      .catch(err => console.error(err))
      this.router.navigate(['/admin']);
    }
  }

  destroy(){
    this.crudService.delete(Model.SEDE, this.id)
    .catch(err => console.error(err))
  }

}
