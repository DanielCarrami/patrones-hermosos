import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsesorFormComponent } from './instructora-form.component';

describe('AsesorFormComponent', () => {
  let component: AsesorFormComponent;
  let fixture: ComponentFixture<AsesorFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsesorFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsesorFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
