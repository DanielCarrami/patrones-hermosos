import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { CrudService, Model } from '../../../services/crud.service';
import {Router, ActivatedRoute} from '@angular/router'
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {NotifySuccessComponent} from '../../dialogs/notify-success/notify-success.component';

interface Options {
  value: string;
  viewValue: string;
}

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-asesor-form',
  templateUrl: './instructora-form.component.html',
  styleUrls: ['./instructora-form.component.css']
})
export class AsesorFormComponent implements OnInit {
  selectedValueGrad: string;
  selectedValueSem: string;
  selectedValueSede: string;

  entra = true;

  no_edit: boolean = true;
  id: any = '';

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  nombre = new FormControl('', [
    Validators.required
  ]);

  apellido = new FormControl('', [
    Validators.required
  ]);

  telefono = new FormControl('', [
    Validators.required
  ]);

  edad = new FormControl('', [
    Validators.required
  ]);

  estado = new FormControl('', [
    Validators.required
  ]);

  ciudad = new FormControl('', [
    Validators.required
  ]);

  carrera = new FormControl('', [
    Validators.required
  ]);

  anio_graduacion = new FormControl('', [
    Validators.required
  ]);

  semestre_carrera = new FormControl('', [
    Validators.required
  ]);

  universidad = new FormControl('', [
    Validators.required
  ]);

  razon_carrera = new FormControl('', [
    Validators.required
  ]);

  razon_participacion = new FormControl('', [
    Validators.required
  ]);

  sede = new FormControl('', [
    Validators.required
  ]);

  sedes: Options[] = [];

  matcher = new MyErrorStateMatcher();

  constructor(
    private crudService: CrudService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {

    this.crudService.get_sede_view(Model.SEDE)
      .then(res =>{
        this.sedes = res.data.map(sede => {
          return {
            value: sede.id,
            viewValue: sede.nombre_institucion
          }
        })
      })
      .catch(err => {
        console.error('EERRROOOOOOOORRRR ' + err);
      });
      
      this.id = this.route.snapshot.paramMap.get('id');
      if(this.id != undefined){
        this.no_edit = false;
        this.crudService.retrieve(Model.TEACHING, this.id)
        .then(res => this.asesor = res.data)
        .catch(err => console.error(err))
      }
  }

  openDialog(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      model: "Instructora",
      name: this.asesor.nombre + ' ' + this.asesor.apellido
    }

    this.dialog.open(NotifySuccessComponent, dialogConfig);
  }

  anios: Options[] = [
    {value: '2020', viewValue: '2020'},
    {value: '2019', viewValue: '2019'},
    {value: '2018', viewValue: '2018'},
    {value: '2017', viewValue: '2017'},
    {value: '2016', viewValue: '2016'},
    {value: '2015', viewValue: '2015'},
    {value: '2014', viewValue: '2014'},
    {value: '2013', viewValue: '2013'},
    {value: '2012', viewValue: '2012'},
    {value: '2011', viewValue: '2011'},
    {value: '2010', viewValue: '2010'}
  ];

  semestres: Options[] = [
    {value: 'Graduada', viewValue: 'Graduada'},
    {value: 'Primero', viewValue: 'Primero'},
    {value: 'Segundo', viewValue: 'Segundo'},
    {value: 'Tercero', viewValue: 'Tercero'},
    {value: 'Cuarto', viewValue: 'Cuarto'},
    {value: 'Quinto', viewValue: 'Quinto'},
    {value: 'Sexto', viewValue: 'Sexto'},
    {value: 'Septimo', viewValue: 'Septimo'},
    {value: 'Octavo', viewValue: 'Octavo'},
    {value: 'Noveno', viewValue: 'Noveno'},
    {value: 'Decimo', viewValue: 'Decimo'},
  ]

  asesor = {
    nombre: '',
    apellido: '',
    email:'',
    telefono:'',
    edad: 0,
    estado:'',
    ciudad:'',
    carrera:'',
    anio_graduacion:2010,
    semestre_carrera:'',
    universidad:'',
    sede_id: '',
    razon_carrera:'',
    razon_participacion:'',
    disp_cambio_sede: false,
    experiencia_previa:false,
    disponibilidad_hospedaje:false,
    aviso_privacidad:false
  }

  verificador(){
    if(this.asesor.nombre === ''|| this.asesor.apellido === '' || this.asesor.telefono === '' || this.asesor.edad === 0 || this.asesor.estado === '' || this.asesor.ciudad === '' || this.asesor.carrera === '' || this.asesor.semestre_carrera === '' || this.asesor.universidad === '' || this.asesor.razon_carrera === '' || this.asesor.razon_participacion === ''){
      this.entra = false
    } else {
      this.entra = true
    }

  }

  ingresar(){
    if(this.no_edit){
        this.verificador()
      if(this.entra == true){
        console.log(this.asesor)
        this.crudService.create(Model.TEACHING, this.asesor);
        this.openDialog();
        this.router.navigate(['/inicio']);
      }
    }else{
      this.crudService.update(Model.TEACHING, this.id, this.asesor)
      .catch(err => console.error(err))
      this.router.navigate(['/admin']);
    }
  }

  destroy(){
    this.crudService.delete(Model.TEACHING, this.id)
    .catch(err => console.error(err))
  }
}
