import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizacionesMensualesComponent } from './actualizaciones-mensuales.component';

describe('ActualizacionesMensualesComponent', () => {
  let component: ActualizacionesMensualesComponent;
  let fixture: ComponentFixture<ActualizacionesMensualesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizacionesMensualesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizacionesMensualesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
