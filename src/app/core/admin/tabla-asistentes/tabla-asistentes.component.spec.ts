import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaAsistentesComponent } from './tabla-asistentes.component';

describe('TablaAsistentesComponent', () => {
  let component: TablaAsistentesComponent;
  let fixture: ComponentFixture<TablaAsistentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaAsistentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaAsistentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
