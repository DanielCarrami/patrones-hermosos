import { Component, OnInit} from '@angular/core';
import { CrudService, Model } from '../../../services/crud.service';
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {WarningDeleteComponent} from '../../dialogs/warning-delete/warning-delete.component';
import {WarningAcceptComponent} from '../../dialogs/warning-accept/warning-accept.component';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-tabla-asistentes',
  templateUrl: './tabla-asistentes.component.html',
  styleUrls: ['./tabla-asistentes.component.css']
})
export class TablaAsistentesComponent implements OnInit {
  sede: string;
  asistentes: any[] = [];
  sedes: any[];
  fileName='Asistentes.xlsx';
  constructor(
    private crudService: CrudService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.crudService.get_sede_view(Model.SEDE)
    .then(res =>{
      this.sedes = res.data.map(sede => {
        return {
          value: sede.id,
          viewValue: sede.nombre_institucion
        }
      })
    })
    .catch(err => {
      console.error('EERRROOOOOOOORRRR ' + err);
    });
        this.crudService.get_all(Model.ASISTENTE)
        .then(res => {
        this.asistentes = res.data;
        })
        .catch(err => {
        console.log(err);
      });
  }

  exportExcel(): void{
    if(this.sede != undefined){
      this.fileName = 'Assitentes' + this.sedes[+this.sede - 1].viewValue + '.xlsx'
    }
    let element = document.getElementById('excel_table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Asistentes');
    XLSX.writeFile(wb, this.fileName);
  }

  formatDate(date): string{
    const year = date.substring(0, 4);
    const month = date.substring(5, 7);
    const day = date.substring(8, 10);
    return day+ '/' + month + '/' + year;
  }
  openWarnDialog(id) {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      model: "Asistente"
    }

    this.dialog.open(WarningDeleteComponent, dialogConfig)
    .afterClosed()
    .subscribe(result => {
      if(result){
        this.crudService.delete(Model.ASISTENTE, id).catch(err => console.error(err))
      }
    });
    location.reload();
  }

  openAcceptDialog(id, nombre) {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      model: "Asistente",
      name: nombre
    }

    this.dialog.open(WarningAcceptComponent, dialogConfig)
    .afterClosed()
    .subscribe(result => {
      if(result){
        this.crudService.update(Model.ASISTENTE, id, {autorizado: true}).catch(err => console.error(err))
      }
    });
    location.reload();
  }

}
