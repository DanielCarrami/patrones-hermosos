import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../services/auth.service';
@Component({
  selector: 'app-tabla-usuarios',
  templateUrl: './tabla-usuarios.component.html',
  styleUrls: ['./tabla-usuarios.component.css']
})
export class TablaUsuariosComponent implements OnInit {

  constructor(private authService: AuthService) { }

  users = [];

  ngOnInit(): void {
    this.authService.showusers().subscribe(
      res=>{this.users = res; console.log(res)}
    );
  }

  eliminarUsuario(id){
    this.authService.eliminarusuarios({usuario_id: id}).subscribe();
    location.reload();
  }

  confirmarUsuario(id){
    this.authService.confirmarusuario({usuario_id: id}).subscribe();
    location.reload();
  }
}
