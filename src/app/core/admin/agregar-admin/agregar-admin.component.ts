import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router'

import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-agregar-admin',
  templateUrl: './agregar-admin.component.html',
  styleUrls: ['./agregar-admin.component.css']
})
export class AgregarAdminComponent implements OnInit {

  user = {
    email: '',
    clave: '',
    nombre:'',
    apellido: ''
  }

  errors = {
    passwordMatch: null,
    notEmpty: null
  }

  clave2: string;

  constructor(
    private authService: AuthService
  ) { }
  
  mensaje_error = '';

  existente = false;
  
  emailControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  nombreControl = new FormControl('', [
    Validators.required
  ]);

  apellidoControl = new FormControl('', [
    Validators.required
  ]);

  passwordControl = new FormControl('', [
    Validators.required
  ]);

  ngOnInit(): void {
  }

  verifyEmpy(): boolean{
    if(this.user.nombre === '' || this.user.email === '' || this.user.clave === '' || this.user.apellido === ''){
      this.errors.notEmpty = 'Todos los campos deben ser llenados'
      return false;
    }
    this.errors.notEmpty = null;
    return true;
  }

  registrarse(){
    if(this.verifyEmpy){
      if(this.clave2 === this.user.clave){
        this.authService.agregaradmin(this.user)
        .subscribe(
        res =>{
          console.log(res)
        }
      )
      }else{
        this.errors.passwordMatch = 'Las contraseñas deben coincidir';
      }
      
    }
  }

 

}
