import { Component, OnInit, Input} from '@angular/core';
import { CrudService, Model } from '../../../services/crud.service';
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {WarningDeleteComponent} from '../../dialogs/warning-delete/warning-delete.component';
import {WarningAcceptComponent} from '../../dialogs/warning-accept/warning-accept.component';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-tabla-sede',
  templateUrl: './tabla-sede.component.html',
  styleUrls: ['./tabla-sede.component.css']
})
export class TablaSedeComponent implements OnInit {
  sedes: any[] = [];
  fileName='Sedes.xlsx';

  constructor(
    private crudService: CrudService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.crudService.get_all(Model.SEDE)
    .then(res => {
      this.sedes = res.data;
    })
    .catch(err =>{
      console.log(err);
    })
  }
  
  exportExcel(): void{
    let element = document.getElementById('excel_table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Instructora');
    XLSX.writeFile(wb, this.fileName);
  }

  openWarnDialog(id) {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      model: "Sede"
    }

    this.dialog.open(WarningDeleteComponent, dialogConfig)
    .afterClosed()
    .subscribe(result => {
      if(result){
        this.crudService.delete(Model.SEDE, id).catch(err => console.error(err))
      }
    });
    location.reload();
  }

  openAcceptDialog(id, nombre) {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      model: "Sede",
      name: nombre
    }

    this.dialog.open(WarningAcceptComponent, dialogConfig)
    .afterClosed()
    .subscribe(result => {
      if(result){
        this.crudService.update(Model.SEDE, id, {autorizado: true}).catch(err => console.error(err))
      }
    });
    location.reload();
  }


}
