import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'sede_filter'
})
export class SedeFilterPipe implements PipeTransform{
    transform(list: any[], value: string){
        return value ? list.filter(item=> item.sede_id == value) : list;
    }
}