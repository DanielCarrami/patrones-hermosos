import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-crear-noticia',
  templateUrl: './crear-noticia.component.html',
  styleUrls: ['./crear-noticia.component.css']
})
export class CrearNoticiaComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  noticia ={
    titulo: '',
    contenido:'',
    imagen:''
  }

  tituloControl = new FormControl('', [Validators.required]);
  contenidoControl = new FormControl('', [Validators.required]);
  imagenControl = new FormControl('', [Validators.required]);
  
  publicar(){
    this.authService.agregarNoticia(this.noticia)
    .subscribe(
      res =>{
        console.log(res)
      }
    )
    location.reload();
  }
}
