import { Component, OnInit, Input } from '@angular/core';
import { CrudService, Model } from '../../../services/crud.service';
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {WarningDeleteComponent} from '../../dialogs/warning-delete/warning-delete.component';
import {WarningAcceptComponent} from '../../dialogs/warning-accept/warning-accept.component';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-tabla-instructora',
  templateUrl: './tabla-instructora.component.html',
  styleUrls: ['./tabla-instructora.component.css']
})
export class TablaAsesoresComponent implements OnInit {

  sede: string;
  asesoras: any[] = [];
  sedes: any[];
  fileName='Instructoras.xlsx';

  constructor(
    private crudService: CrudService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.crudService.get_sede_view(Model.SEDE)
    .then(res =>{
      this.sedes = res.data.map(sede => {
        return {
          value: sede.id,
          viewValue: sede.nombre_institucion
        }
      })
    })
    .catch(err => {
      console.error('EERRROOOOOOOORRRR ' + err);
    });

    this.crudService.get_all(Model.TEACHING)
    .then(res => this.asesoras = res.data)
    .catch(err => {
    console.log(err);
    });
  }

  exportExcel(): void{
    if(this.sede != undefined){
      this.fileName = 'Instructora' + this.sedes[+this.sede - 1].viewValue + '.xlsx'
    }
    let element = document.getElementById('excel_table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Instructora');
    XLSX.writeFile(wb, this.fileName);
  }

  openWarnDialog(id) {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      model: "Instructora"
    }

    this.dialog.open(WarningDeleteComponent, dialogConfig)
    .afterClosed()
    .subscribe(result => {
      if(result){
        this.crudService.delete(Model.TEACHING, id).catch(err => console.error(err))
      }
    });
    location.reload();
  }

  openAcceptDialog(id, nombre) {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      model: "Instructora",
      name: nombre
    }

    this.dialog.open(WarningAcceptComponent, dialogConfig)
    .afterClosed()
    .subscribe(result => {
      if(result){
        this.crudService.update(Model.TEACHING, id, {autorizado: true}).catch(err => console.error(err))
      }
    });
    location.reload();
  }

}
