import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaAsesoresComponent } from './tabla-instructora.component';

describe('TablaAsesoresComponent', () => {
  let component: TablaAsesoresComponent;
  let fixture: ComponentFixture<TablaAsesoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaAsesoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaAsesoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
