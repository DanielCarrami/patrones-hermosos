import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: AuthService) { }
  Boolean = false;
  ngOnInit(): void {
    this.authService.verifyadmin()
    .subscribe(
      res => {
        if (res == true){
          localStorage.setItem('admin',res);
        }
        
      },
      err => this.authService.logoutadmin()
      
    )
  }

  adminverify(){
    if(localStorage.getItem("admin") != null){
      return true;
    }
    else{
    
      return false;
    }
  }

  loggedIn(){
    return !!localStorage.getItem('token');
  }
  logOut(){
    this.authService.logout()
    this.authService.logoutadmin()
    location.reload();
    
  }


}
