import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarningInscripcionComponent } from './warning-inscripcion.component';

describe('WarningInscripcionComponent', () => {
  let component: WarningInscripcionComponent;
  let fixture: ComponentFixture<WarningInscripcionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarningInscripcionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarningInscripcionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
