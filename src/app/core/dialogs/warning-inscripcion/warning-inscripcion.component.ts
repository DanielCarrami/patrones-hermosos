import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
@Component({
  selector: 'app-warning-inscripcion',
  templateUrl: './warning-inscripcion.component.html',
  styleUrls: ['./warning-inscripcion.component.css']
})
export class WarningInscripcionComponent implements OnInit {
  error: string = "";
  nombre: string;
  constructor(
    public dialogRef: MatDialogRef<WarningInscripcionComponent>,
    @Inject(MAT_DIALOG_DATA
      ) data) { 
        this.error = data.error;
        this.nombre = data.info;
      }

  ngOnInit(): void {
  }

}
