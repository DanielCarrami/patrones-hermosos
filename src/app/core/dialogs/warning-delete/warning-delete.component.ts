import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-warning-delete',
  templateUrl: './warning-delete.component.html',
  styleUrls: ['./warning-delete.component.css']
})
export class WarningDeleteComponent implements OnInit {

  model: string;
  constructor(
    public dialogRef: MatDialogRef<WarningDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) data
    ) {
      this.model = data.model;
     }

  ngOnInit(): void {
  }

  close() {
    this.dialogRef.close();
  }

}
