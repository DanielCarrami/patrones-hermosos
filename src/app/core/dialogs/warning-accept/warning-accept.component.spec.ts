import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarningAcceptComponent } from './warning-accept.component';

describe('WarningAcceptComponent', () => {
  let component: WarningAcceptComponent;
  let fixture: ComponentFixture<WarningAcceptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarningAcceptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarningAcceptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
