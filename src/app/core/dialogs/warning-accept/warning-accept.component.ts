import { Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-warning-accept',
  templateUrl: './warning-accept.component.html',
  styleUrls: ['./warning-accept.component.css']
})
export class WarningAcceptComponent implements OnInit {

  model: string;
  name: string;
  constructor(
    public dialogRef: MatDialogRef<WarningAcceptComponent>,
    @Inject(MAT_DIALOG_DATA) data
    ) {
      this.model = data.model;
      this.name = data.name;
     }

  ngOnInit(): void {
  }

  close() {
    this.dialogRef.close();
  }

}
