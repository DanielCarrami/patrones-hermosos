import { Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-fail-login',
  templateUrl: './fail-login.component.html',
  styleUrls: ['./fail-login.component.css']
})
export class FailLoginComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<FailLoginComponent>
  ) { }

  ngOnInit(): void {
  }

}
