import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-notify-success',
  templateUrl: './notify-success.component.html',
  styleUrls: ['./notify-success.component.css']
})
export class NotifySuccessComponent implements OnInit {
  model: string;
  name: string;

  constructor(
    public dialogRef: MatDialogRef<NotifySuccessComponent>,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    this.model = data.model; 
    this.name = data.nombre;
   }

  ngOnInit(): void {
  }

}
