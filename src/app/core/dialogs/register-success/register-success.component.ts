import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-register-success',
  templateUrl: './register-success.component.html',
  styleUrls: ['./register-success.component.css']
})
export class RegisterSuccessComponent implements OnInit {

  name: string;

  constructor(public dialogRef: MatDialogRef<RegisterSuccessComponent>,
    @Inject(MAT_DIALOG_DATA) data) { 
      this.name = data.name;
    }

  ngOnInit(): void {
  }

}
