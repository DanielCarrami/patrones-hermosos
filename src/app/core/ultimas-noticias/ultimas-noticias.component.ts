import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router'
@Component({
  selector: 'app-ultimas-noticias',
  templateUrl: './ultimas-noticias.component.html',
  styleUrls: ['./ultimas-noticias.component.css']
})
export class UltimasNoticiasComponent implements OnInit {

  
  noticias1 ;
  noticias2 ;
  noticias3 ;
  noticias = [];

  agregarlike = {
    id: 0,
    likes:0
  }
  
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.authService.Allnews()
    .subscribe(
      res =>{
        this.noticias = res;
        this.noticias1 = res.length - 1;
        this.noticias2 = res.length-2;
        this.noticias3 = res.length-3;

      }
    )
  }

  enviarnoticia(id){
    localStorage.setItem('noticia',id);
    this.router.navigate(['/noticia-detalle']);
  }

  likear(id,numlikes){
    this.agregarlike.id = id;
    this.agregarlike.likes = numlikes + 1;

    this.authService.updatelike(this.agregarlike)
      .subscribe(
        res =>{
  
        }
      )

    location.reload()


  }

}