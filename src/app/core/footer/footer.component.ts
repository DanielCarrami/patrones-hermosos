import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { CrudService, Model } from '../../services/crud.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  mailingListForm;

  constructor(
    private formBuilder: FormBuilder,
    private crudService: CrudService
  ) {
    this.mailingListForm = this.formBuilder.group({
      nombre: '',
      email: '',
      politicas_privacidad: ''
    });
   }

  ngOnInit(): void {
  }

  onSubmit(mailData){
    this.crudService.create(Model.MAILING_list,mailData);
    this.mailingListForm.reset();
  }

}
