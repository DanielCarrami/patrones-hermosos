import { Component } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'patrones-hermosos';

  showHead: boolean = false;

  constructor(private router:Router){
    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        if ((event['url'] == '/formulario-sede') || (event['url'] == '/login') || (event['url'] == '/register') || (event['url'] == '/formulario-instructora') || (event['url'] == '/formulario-asistente'))  {
          this.showHead = false; 
        } else {
          this.showHead = true;
        }
      }
    });
  }
}
