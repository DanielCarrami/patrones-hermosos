import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http'
import {AuthGuard} from './services/auth.guard';
import {FormsModule} from "@angular/forms";
import {ReactiveFormsModule} from "@angular/forms";
import {Router, Scroll, Event} from '@angular/router'
import {ViewportScroller} from '@angular/common';
import { filter } from 'rxjs/operators';

import { AppRoutingModule } from './app-routing.module';
import { MatSliderModule } from '@angular/material/slider';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu'; 
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from "@angular/material/dialog";
import {MatIconModule} from '@angular/material/icon';
import {MatRadioModule} from '@angular/material/radio';

import { AppComponent } from './app.component';
import { InicioDashboardComponent } from './pages-components/inicio-dashboard/inicio-dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AboutPageComponent } from './pages-components/about-page/about-page.component';
import { ParticipaPageComponent } from './pages-components/participa-page/participa-page.component';
import { InscribetePageComponent } from './pages-components/inscribete-page/inscribete-page.component';
import { NoticiasPageComponent } from './pages-components/noticias-page/noticias-page.component';
import { ContactoPageComponent } from './pages-components/contacto-page/contacto-page.component';
import { HeaderComponent } from './core/header/header.component';
import { EquipoPageComponent } from './pages-components/equipo-page/equipo-page.component';
import { PatrocinadoresPageComponent } from './pages-components/patrocinadores-page/patrocinadores-page.component';
import { FooterComponent } from './core/footer/footer.component';
import { ActualizacionesMensualesComponent } from './core/actualizaciones-mensuales/actualizaciones-mensuales.component';
import { UltimasNoticiasComponent } from './core/ultimas-noticias/ultimas-noticias.component';
import { LoginPageComponent } from './pages-components/login-page/login-page.component';
import { AgregarnoticiasPageComponent } from './pages-components/agregarnoticias-page/agregarnoticias-page.component';
import {TokenInterceptorService} from './services/token-interceptor.service';
import { NoticiaPageComponent } from './pages-components/noticia-detalle-page/noticia-detalle-page.component';
import { RegisterPageComponent } from './pages-components/register-page/register-page.component';
import { AdminPageComponent } from './pages-components/admin-page/admin-page.component';
import { SedeFormComponent } from './core/forms/sede-form/sede-form.component';
import { AsesorFormComponent } from './core/forms/instructora-form/instructora-form.component';
import { AsistenteFormComponent } from './core/forms/asistente-form/asistente-form.component';
import { TablaSedeComponent } from './core/admin/tabla-sede/tabla-sede.component';
import { TablaAsistentesComponent } from './core/admin/tabla-asistentes/tabla-asistentes.component';
import { TablaAsesoresComponent } from './core/admin/tabla-instructora/tabla-instructora.component';
import { WarningDeleteComponent } from './core/dialogs/warning-delete/warning-delete.component';
import { WarningAcceptComponent } from './core/dialogs/warning-accept/warning-accept.component';
import { NotifySuccessComponent } from './core/dialogs/notify-success/notify-success.component';
import { SedeFilterPipe } from './core/admin/filter-pipe/sede-filter.pipe';
import { WarningInscripcionComponent } from './core/dialogs/warning-inscripcion/warning-inscripcion.component';
import { RedesSocialesComponent } from './core/redes-sociales/redes-sociales.component';
import { CrearNoticiaComponent } from './core/admin/crear-noticia/crear-noticia.component';
import { AgregarAdminComponent } from './core/admin/agregar-admin/agregar-admin.component';
import { TablaUsuariosComponent } from './core/admin/tabla-usuarios/tabla-usuarios.component';
import { RegisterSuccessComponent } from './core/dialogs/register-success/register-success.component';
import { FailLoginComponent } from './core/dialogs/fail-login/fail-login.component';

@NgModule({
  declarations: [
    AppComponent,
    InicioDashboardComponent,
    AboutPageComponent,
    ParticipaPageComponent,
    InscribetePageComponent,
    NoticiasPageComponent,
    ContactoPageComponent,
    HeaderComponent,
    EquipoPageComponent,
    PatrocinadoresPageComponent,
    FooterComponent,
    ActualizacionesMensualesComponent,
    UltimasNoticiasComponent,
    LoginPageComponent,
    AgregarnoticiasPageComponent,
    NoticiaPageComponent,
    RegisterPageComponent,
    AdminPageComponent,
    SedeFormComponent,
    AsesorFormComponent,
    AsistenteFormComponent,
    TablaSedeComponent,
    TablaAsistentesComponent,
    TablaAsesoresComponent,
    WarningDeleteComponent,
    WarningAcceptComponent,
    NotifySuccessComponent,
    SedeFilterPipe,
    WarningInscripcionComponent,
    RedesSocialesComponent,
    CrearNoticiaComponent,
    AgregarAdminComponent,
    TablaUsuariosComponent,
    RegisterSuccessComponent,
    FailLoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatToolbarModule,
    MatMenuModule,
    MatCheckboxModule,
    MatButtonModule,
    FormsModule,
    HttpClientModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    FormsModule,
    MatTableModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatIconModule,
    MatRadioModule,
    MatDialogModule
  ],
  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    WarningDeleteComponent
  ]
})
export class AppModule {
  constructor(router: Router, viewportScroller: ViewportScroller) {
    router.events.pipe(
      filter((e: Event): e is Scroll => e instanceof Scroll)
    ).subscribe(e => {
      if (e.position) {
        // backward navigation
        viewportScroller.scrollToPosition(e.position);
      } else if (e.anchor) {
        // anchor navigation
        viewportScroller.scrollToAnchor(e.anchor);
      } else {
        // forward navigation
        viewportScroller.scrollToPosition([0, 0]);
      }
    });
  }
}
