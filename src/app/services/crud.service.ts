import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import axios from 'axios';

export enum Model{
    TEACHING = 'teaching',
    ASISTENTE = 'asistente',
    TUTOR = 'tutor',
    CONTACTO_CAPTACION = 'contacto_captacion',
    PROGRAMAS_INTERNACIONALES = 'programas_internacionales',
    RESPONSABLE_ACADEMICO = 'responsable_academico',
    SEDE = 'sede',
    MAILING_list = 'mailing_list',
}
@Injectable({
  providedIn: 'root'
})
export class CrudService {
  URL: string;
  headers: HttpHeaders;

  constructor(private http: HttpClient) { 
      this.URL = environment.baseUrl;
      this.headers = new HttpHeaders({
        "Content-Type": "application/json"
      });
  }

    /**
   *
   * @param model The model to list
   * @param searchParams Optional search parameters for the query.
   * Filters (i.e. SQL 'WHERE' clauses) can be added here.
   */
  list(model: Model, searchParams?: URLSearchParams){
    let url = this.URL + '/' + model;
    if (searchParams) {
      url += "?" + searchParams.toString();
    }
    return axios.get(
      url
    );
  }

  get_all(model: Model, params?) {
    let url = this.URL + '/' + model
    return axios.get(url, {
      params
    });
  }

  get_sede_view(model: Model){
    let url = this.URL + '/' + model + '/id_name';
    return axios.get(url);
  }

  create(model: Model, body?) {
    let url = this.URL + "/" + model;
    return axios.post(url, body);
  }
  
  help_sede(model: Model, params?){
    let url = this.URL + "/" + model +"/apoyo_sede/" + params.toString();
    return axios.post(url);
  }
  //get-one
  retrieve(model: Model, id: number) {
    let url = this.URL + "/" + model + "/" + id;
    return axios.get(url);
  }

  delete(model: Model, id: any) {
    let url = this.URL + "/" + model + "/" + id;
    return axios.delete(url);
  }

  update(model: Model, id: any, body?){
    let url = this.URL + "/" + model + "/update/" + id;
    return axios.put(url, body)
  }
}
