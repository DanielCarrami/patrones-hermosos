import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private URL = 'http://localhost:3001'
  
  headers: HttpHeaders;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      "Content-Type": "application/json"
    });
   }
  
  //Manda a ver si el usuario y contraseña es correcto
  signIn(user){
    return this.http.post<any>(this.URL + '/signin',user);
  }

  //Checa si el usuario esta registrado
  loggedIn(){
    return !!localStorage.getItem('token');
  }

  //Toma todas las noticias
  Allnews(){
    return this.http.post<any>(this.URL + '/todasnoticias',{});
  }

  //Toma todas las noticias ordenadas de mas nueva a mas vieja
  AllnewsOrdenadas(){
    return this.http.post<any>(this.URL + '/nuevasnoticias',{});
  }

  getToken(){
    return localStorage.getItem('token');
  }
  logout(){
    localStorage.removeItem('token');
  }
  logoutadmin(){
    localStorage.removeItem('admin');
  }

  //Consigue una noticia
  getOneNews(noticia){
    return this.http.post<any>(this.URL + '/unanoticia',noticia);
  }

  comentar(comentario){
    return this.http.post<any>(this.URL + '/addcomment',comentario);
  }
  
  //Consigue todas las noticias con un titulo en especial
  busquedaNoticia(titulo){
    return this.http.post<any>(this.URL + '/buscarnombre',titulo);
  }

  //Agrega una visita mas a una noticia
  updateViews(views){
    return this.http.post<any>(this.URL + '/views',views);
  }

  //Verifica que el usuario es un admin
  verifyadmin(){
    return this.http.post<any>(this.URL + '/admin',{});
  }

  //Agregar noticia
  agregarNoticia(noticia){
    return this.http.post<any>(this.URL + '/add',noticia);
  }

  //Agrega un nuevo usuario
  agregarusuario(usuario){
    return this.http.post<any>(this.URL + '/adduser',usuario);
  }

  deletecomment(comment_delete){
    return this.http.post<any>(this.URL + '/deletecomment',comment_delete);
  
  }

  respuestacomment(respuesta){
    return this.http.post<any>(this.URL + '/addresponse',respuesta);
  }

  updatelike(likes){
    return this.http.post<any>(this.URL + '/likes',likes);
  }

  //Lista de todos los usuarios
  showusers(){
    return this.http.post<any>(this.URL + '/usuarioscompleto',{});
  }

  //Agrega admin
  agregaradmin(admin){
    return this.http.post<any>(this.URL + '/addadmin',admin);
  }
  
  //Elimina usuario basado en el id
  eliminarusuarios(usuario){
    return this.http.post<any>(this.URL + '/deleteuser',usuario);
  }

  //Cambiar estatus de usuario de no confirmado a confirmado
  confirmarusuario(usuario){
    return this.http.post<any>(this.URL + '/updatestatus',usuario);
  }

}
