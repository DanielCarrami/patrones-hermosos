# PatronesHermosos

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.4.

Patrones Hermosos es un programa del MIT para impulsar motivar mujeres jóvenes a seguir una carrera en el área de Tecnologías de la información y comunicación

Esta aplicación es el frontend del sitio web oficial

Fue creado por Daniel Carramiñana Carrasco y Alarico Mercado Vázquez

## Instalación

Es necesario tener angular instlado, de no tenerlo instalarlo utilizando npm en su última versión

```
npm install -g @angular/cli
```

Posteriormente dentro de la carpeta del proyecto correr las instalaciones de los paquetes de Node

```
npm install
```

## Ejecutar aplicación localmente

Para correr la aplicación

```
ng serve
```

Esperar a que se realice la compilación y vistar http://localhost:4200

## Troubleshoting

Si se tienen problemas para correr el programa por que no se reconoce el comando ng, NO INSTALE ng-common como suele mencionar el error que marca la terminal, simplemente corra el siguiente comando

```
npm upgrade
```

Y listo

## Despliegue (Deployment)

Una vez que se quiera desplegar la aplicación, con las configuraciones necesarias para el servidor, dependiendo del cliente, siguiendo la documentación oficial de Angular en [su documentación oficial](https://angular.io/guide/deployment) es necesario correr el siguiente comando:

```
ng build --prod
```

El comando generará una nueva carpeta (/dist), de la cual se debe copiar todo el contenido en una carpeta dentro del servidor. Es necesario realizar las configuraciones pertinentes del servidor para redireccionar las peticiones cuando se solicitan archivos que no existen.
